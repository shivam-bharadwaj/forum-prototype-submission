# Forum Submission Prototype - Elite Forum

This repo contains the source code which is currently deployed on [Google COmpute Engine](http://35.185.239.176/) meant for #UnitedByHCL Hackathon. This not the Git maintained Repository of Debug version but a mere replica of that, we maintained the code for hackathon project via a 'private' repository.

### Deployed Prototype

*URL* - [http://35.185.239.176/](http://35.185.239.176/demo/elite-forum/)
*Updated URL* - [Prototype-Demo](http://shivambharadwaj.com/demo/elite-forum/)

for testing there is a dummy user with following credentials
> username: dummy_user_1
> password: testpassword

and also a Admin User who can access everything at [Admin URL](http://35.185.239.176/admin/) with following credentials, an admin currently CANNOT post queries.
> username: shivam
> password: testpassword


## About Project

The Project is basically a Django Powered Project, and currently only web based front end is built ( which is again in development).
The Project is currently in active development and may contain bugs, broken links.

The repository however contains almost every script tested and written so far to test the said functionalities.
The project works perfectly fine for testing purpose, however not suited for production usage, to run and test follow installation instructions.

## Technology Stack & Frameworks Used So far

 * [Django](https://www.djangoproject.com) - for Backend Processing
 * [PostgreSQL](https://www.postgresql.org) - for Database
 * [Tweepy](http://www.tweepy.org) - for Using Twitter Streaming API, Fetching Tweets
 * [Django CKEditor](https://github.com/django-ckeditor/django-ckeditor) - for Provinding Feature Rich Text Editor
 * [MDBootstrap](https://mdbootstrap.com) - for developing the front end of web interface
 * [nginx](https://nginx.org/en/) - As a Web Server on the COmpute Engine
 * [Gunicorn](http://gunicorn.org) - As an app server for Serving Django Project on Compute Engine

## Structure : Files

The Project follows basic Structure of Django Project.
```
├── ForumPrototype5/
├── all_train_data.csv
├── app_models/
├── logfile
├── manage.py
├── media/
├── nginx.conf
├── open_apis/
├── readme.md
├── requirements.txt
├── startgunicorn.py
├── static/
└── web_app/
```
Directories Explained :-

ForumPrototype5/ is project directory containing ususla urls.py settings.py utils.py and wsgi.py

ForumPrototype5/

├── __init__.py

├── __pycache__

├── settings.py

├── urls.py

├── utils.py

└── wsgi.py

open_apis, web_app, app_models are app directory of Django project.

open_apis is for future development of APIs for mobile platforms

web_app contains forms, templates and views to display the app on web platform

app_models is the core app containg model details used in forum project.

static and media directories are supposed to serve and store static and media files.

# Structure : Database

PostgreSQL has been used as Database (server ) for it.
Database has following tables to store details of forum functioning :

UserProfileInfo - for storing profile details of user

Tags - for storing tags

Services - for storing Category  / Services of query

QueriesModel - storing Query, including
 description and automated answer_summary

AnswersModel - to store Answers

DiscussionThreadsSolutionModel - to store any replies or discussion done on a particular solution / answer

DiscussionThreadsQueryModel - to store any replies or discussion done on a particular query / question

UpvotesModel and DownvotesModel

NotificationsModel

TrainDataSet - for storing taring dataset for machine learning comonent for generating summary

## installation instructions

The project is derived from development version for deployment and hence contains migrations already done so to get started, you need to either migrate to ZERO or remove folders __pycache__ and migrations from each subdirectory.

Secondly it requires a Postgres Database.

#### Resolving Dependencies

    1. (optional but recommended )create a virtual environment, activate it

    2. goto project root directory, and open terminal there.

    3. run following commands in termina

        * `sudo -H pip install -r ./requirements.txt`
        * `sudo -H pip install textblob`
        * `sudo -H pip install tweepy`

#### For serving a test version

  1. Configure a PostGresQl server
  2. use the setting for this server and database created to link it to Django Project in ForumPrototype5/setting.py file
  3. Open Terminal in project root directory
  4. run commands

    * ```python manage.py migrate```

    * ```python manage.py makemigrations app_models```

    * ```python manage.py migrate```

  5. run
    * ```python manage.py createsuperuser```

  and follow instrutions

  6. run

    * ```python manage.py runserver```

  and you will see your server running at http://127.0.0.1:8000
  you should see a screen which contains no post as database needs to populated. You can populate the database by loggin to admin at (your_url)/admin and follow onscreen steps.
