from django.contrib import admin
from app_models.models import (AnswersModel,DiscussionThreadsQueryModel,DiscussionThreadsSolutionModel,
                                UserProfileModel,TagsModel,QueriesModel,ServicesModel,TrainDataSet)
# Register your models here.
admin.site.register(AnswersModel)

admin.site.register(DiscussionThreadsQueryModel)

admin.site.register(DiscussionThreadsSolutionModel)

admin.site.register(UserProfileModel)

admin.site.register(TagsModel)

admin.site.register(QueriesModel)

admin.site.register(ServicesModel)

admin.site.register(TrainDataSet)
