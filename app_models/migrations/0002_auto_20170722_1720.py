# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-22 17:20
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answersmodel',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 22, 17, 20, 37, 808432, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='discussionthreadsquerymodel',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 22, 17, 20, 37, 811961, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='discussionthreadssolutionmodel',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 22, 17, 20, 37, 809350, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='queriesmodel',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 22, 17, 20, 37, 806544, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='queriesmodel',
            name='last_modified',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 22, 17, 20, 37, 806689, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='userprofilemodel',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2017, 7, 22, 17, 20, 37, 804128, tzinfo=utc)),
        ),
    ]
