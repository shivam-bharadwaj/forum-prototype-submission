# Model Classes for Table Creation & Management

This directory contains the models in _models.py_ file.

In Layman's terminology, A model in django is simply a table in your database.

For more information on models read (Official Django documentation for Models)[https://docs.djangoproject.com/en/1.11/topics/db/models/].
