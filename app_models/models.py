from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone as django_timezone
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.


class UserProfileModel(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,related_name='profile')
    is_editor = models.BooleanField(default=False)
    services_interested = models.CharField(max_length=300)
    profile_image = models.ImageField(upload_to='users/profile_pics',blank=True)
    created_date = models.DateTimeField(default=django_timezone.now())

    def __str__(self):
        return self.user.username

class TagsModel(models.Model):
    tagName = models.CharField(max_length=264,unique=True,null=False)

    def __str__(self):
        return self.tagName


class ServicesModel(models.Model):
    title = models.CharField(max_length=264)
    description = models.TextField


    def __str__(self):
        return self.title

class QueriesModel(models.Model):
    title = models.CharField("Your Query",max_length=264,unique=True)
    text = RichTextUploadingField("Query Description")
    attachment = models.FileField("Related File",upload_to='queries/attachments',blank=True)
    created_date = models.DateTimeField(default=django_timezone.now())
    created_by = models.ForeignKey(UserProfileModel,related_name='own_queries')
    last_modified = models.DateTimeField(default=django_timezone.now())
    is_spam = models.BooleanField(default=False)
    tags=models.CharField(blank=True,null=True,max_length=300)
    answer_summary = models.TextField(blank=True,null=True)
    tag1 = models.ForeignKey(TagsModel,blank=True,related_name='tag1',null=True)
    tag2 = models.ForeignKey(TagsModel,blank=True,related_name='tag2',null=True)
    tag3 = models.ForeignKey(TagsModel,blank=True,related_name='tag3',null=True)
    tag4 = models.ForeignKey(TagsModel,blank=True,related_name='tag4',null=True)
    tag5 = models.ForeignKey(TagsModel,blank=True,related_name='tag5',null=True)
    service_area = models.ForeignKey(ServicesModel,blank=True,related_name='queries_in_service_area',null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('web_app:ShowQueryPageURL',kwargs={'pk':self.pk})


class AnswersModel(models.Model):
    query = models.ForeignKey(QueriesModel,related_name='answers')
    text = RichTextUploadingField()
    created_date = models.DateTimeField(default=django_timezone.now())
    created_by = models.ForeignKey(UserProfileModel,related_name='answers_posted')
    upvotes = models.PositiveIntegerField(default=0)
    downvotes = models.PositiveIntegerField(default=0)
    is_solution = models.BooleanField(default=False)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        url_string =  reverse('web_app:ShowQueryPageURL',kwargs={'pk':self.query.pk})
        element_id = '#answer'+str(self.pk)
        return url_string+element_id

class DiscussionThreadsSolutionModel(models.Model):
    ans = models.ForeignKey(AnswersModel,related_name='solution_discussion_threads')
    text = models.TextField()
    created_at = models.DateTimeField(default=django_timezone.now())
    created_by = models.ForeignKey(UserProfileModel,related_name='solution_discussions')

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        element_id = 'solution_discussion_thread'+str(self.pk)
        # return reverse('web_app:ShowQueryPageWithElementIDURL',kwargs={'pk':self.query.pk,'elementid':element_id})
        strinURL = reverse('web_app:ShowQueryPageURL',kwargs={'pk':self.ans.query.pk})
        strinURL = strinURL+"#"+element_id
        return strinURL

class DiscussionThreadsQueryModel(models.Model):
    query = models.ForeignKey(QueriesModel,related_name='query_discussion_threads')
    text = models.TextField()
    created_by = models.ForeignKey(UserProfileModel,related_name='query_disussions')
    created_at = models.DateTimeField(default=django_timezone.now())

    def __str__(self):
        return self.text
    def get_absolute_url(self):
        element_id = 'query_discussion_thread'+str(self.pk)
        # return reverse('web_app:ShowQueryPageWithElementIDURL',kwargs={'pk':self.query.pk,'elementid':element_id})
        strinURL = reverse('web_app:ShowQueryPageURL',kwargs={'pk':self.query.pk})
        strinURL = strinURL+"#"+element_id
        return strinURL

class UpvotesModel(models.Model):
    upvoter = models.ForeignKey(UserProfileModel)
    upvoted_answer = models.ForeignKey(AnswersModel)

class DownvotesModel(models.Model):
    downvoter = models.ForeignKey(UserProfileModel)
    downvoted_answer = models.ForeignKey(AnswersModel)

class NotificationsModel(models.Model):
    pass

class TrainDataSet(models.Model):
    query_pk = models.ForeignKey(QueriesModel)
    summary = models.TextField()
    answer = models.TextField()

# class TweetsData(models.Model):
#     fetch_date = models.DateTimeField(default=django_timezone.now())
#     tweet_id = models.CharField()
#     tweet_text = models.TextField()
#     tweet_user_id = models.CharField()
#     sentiment = models.CharField()
#     tweet_created_at = models.DateTimeField()

## Similarly for Others
