"""ForumPrototype5 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.contrib.auth import views as authViews
from web_app.views import login as custom_login

## for media files
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/$',RedirectView.as_view(pattern_name='loginPageURL'),name='loginShortURL'),
    url(r'accounts/login/$',custom_login,{'template_name': 'registration/login-signup.html'},name='loginPageURL'),
    url(r'accounts/logout/$',authViews.logout,name='logoutPageURL',kwargs={'next_page':'/'}),
    url(r'',include('web_app.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
