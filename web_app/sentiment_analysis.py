import re
import tweepy
from tweepy import OAuthHandler
from textblob import TextBlob
import json

### web app models integration


## Author - Shivam Bharadwaj

# Authentication details. To  obtain these visit dev.twitter.com
consumer_key = 'dFY09yaAadck9IhipkvVb9Ovp'
consumer_secret = 'iAkeubK63gze1IODsR2jua7iOKVYLdxhNrQUR0kfHoy7dbfZAn'
access_token = '2323284181-r4TF4ntIYcSPq1PtRyE1JCX4RVoM2ylvJcfep83'
access_token_secret = 'M22qbecFQ98QNKK3AdILZKF1ISxFhtedBl3a8toi5tgvL'


#the Listener
class StdOutListener(tweepy.StreamListener):
    def on_data(self, data):
        # Twitter returns data in JSON format - we need to decode it first
        decoded = json.loads(data)
        user_screen_name = decoded['user']['screen_name']
        user_tweet = decoded['text'].encode('ascii', 'ignore')
        
        # unique_name = decoded['id_str']
        # unique_name +=".txt"
        # fileName = open(unique_name,'w+')
        print(" Unfiltered data is \n ")
        # fileName.write(str(decoded))
        # fileName.close()
        print(decoded)
        print(" ")
        # Also, we convert UTF-8 to ASCII ignoring all bad characters sent by users
        print("{} just posted {} on {}".format(user_screen_name, user_tweet,decoded['created_at']), end=' ')


        analysis = TextBlob(clean_tweet(str(user_tweet)))
        # set sentiment
        if analysis.sentiment.polarity > 0:
            print(" POSITIVE SENTIMENT ")
            # positive_tweets.write(str(user_tweet)+"\n")
        elif analysis.sentiment.polarity == 0:
            print(" Neutral SENTIMENT ")
            # neutral_tweets.write(str(user_tweet)+"\n")
        else:
            print(" Negative SENTIMENT ")
            # negative_tweets.write(str(user_tweet)+"\n")
        return True

    def on_error(self, status):
        print(status)

def clean_tweet(tweet):
    '''
    Utility function to clean tweet text by removing links, special characters
    using simple regex statements.
    '''
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])(\w+:\/\/\S+)", " ", tweet).split())

def launchTweetGrabber(query):
    l = StdOutListener()
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    print("Showing all new tweets for Dryice:")

    # There are different kinds of streams: public stream, user stream, multi-user streams
    # In this example follow #programming tag
    # For more details refer to https://dev.twitter.com/docs/streaming-apis
    stream = tweepy.Stream(auth, l)
    stream.filter(track=[query],async=True)
