from django.conf.urls import url
from web_app.views import (IndexPage,registerNewUser,CreateQueryPage,ShowQueryPage,ShowQueryListByCategory,
                            ShowSearchResult,CreateDiscussionThreadsQueryPage,CreateDiscussionThreadsSolutionPage,
                            CreateAnswerPage,exportCSVtrainData,populate_train_db,UpdateUpvote,UpdateDownvote)

app_name = 'web_app'


urlpatterns = [
url(r'^$',IndexPage.as_view(),name='IndexPageURL'),
url(r'^register/$',registerNewUser,name='RegisterNewuUserURL'),
url(r'^ask/$',CreateQueryPage.as_view(),name='CreateQueryPageURL'),
url(r'^query/(?P<pk>\d+)#(?P<elementid>[a-zA-Z\d]+)$',ShowQueryPage.as_view(),name='ShowQueryPageWithElementIDURL'),
url(r'^query/(?P<pk>\d+)/$',ShowQueryPage.as_view(),name='ShowQueryPageURL'),
url(r'^query/(?P<pk>\d+)/upvote-update-answer/(?P<answerpk>\d+)$',UpdateUpvote.as_view(),name='UpdateUpvoteURL'),
url(r'^query/(?P<pk>\d+)/downvote-update-answer/(?P<answerpk>\d+)$',UpdateDownvote.as_view(),name='UpdateDownvoteURL'),
url(r'^queries/(?P<category>[\w-]+)/$',ShowQueryListByCategory.as_view(),name='ShowQueryListByCategoryURL'),
url(r'^search/$',ShowSearchResult.as_view(),name='InitiateSearchURL'),
url(r'^search/?(?P<search_query>[\w-]+)/$',ShowSearchResult.as_view(),name='ShowSearchResultURL'),
url(r'^create-query-comment/(?P<querypk>\d+)/$',CreateDiscussionThreadsQueryPage.as_view(),name='CreateDiscussionThreadsQueryPageURL'),
url(r'^create-answer-comment/(?P<answerpk>\d+)/(?P<querypk>\d+)$',CreateDiscussionThreadsSolutionPage.as_view(),name='CreateDiscussionThreadsSolutionPageURL'),
url(r'^create-answer/(?P<querypk>\d+)/$',CreateAnswerPage.as_view(),name='CreateAnswerPageURL'),
url(r'^export_training_data/$',exportCSVtrainData,name='ExportCSVTrainDataURL'),
url(r'^populate_training_data/$',populate_train_db,name='PopulateTrainDBURL'),
]
