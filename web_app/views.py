from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView,CreateView,DetailView,ListView,DeleteView,UpdateView
from web_app.forms import UserInfoRegistrationForm,UserProfileInfoRegistrationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from web_app import forms as web_app_forms
from app_models import models as forumModels
from django.urls import reverse,reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.postgres.search import SearchVector,SearchRank,SearchQuery
from django.contrib.auth.views import login as auth_views_login
from django.contrib.auth.forms import AuthenticationForm

from web_app.train import export_to_csv,save_train_data
# Create your views here.

##Landing Page
class IndexPage(ListView):
    model = forumModels.QueriesModel
    template_name = 'web_app/index-new2.html'
    context_object_name = 'query_list'
    paginate_by = 2
    queryset = forumModels.QueriesModel.objects.all().order_by('-created_date')

    def get_context_data(self,**kwargs):
        context = super(IndexPage,self).get_context_data(**kwargs)
        context['tags_list'] = forumModels.TagsModel.objects.all()
        context['search_bar_form'] = web_app_forms.SearchBarForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

#login_view using django auth
def login(*args, **kwargs):
    registration_form1 = web_app_forms.UserInfoRegistrationForm()
    registration_form2 = web_app_forms.UserProfileInfoRegistrationForm()

    extra_context = {'reg_form1': registration_form1, 'reg_form2': registration_form2}
    return auth_views_login(*args, extra_context=extra_context, **kwargs)

## Signing up a new user
def registerNewUser(request):
    registered = False
    errors = False
    err1 = None
    err2 = None


    if request.method == 'POST':
        user_form = UserInfoRegistrationForm(data = request.POST)
        profile_form = UserProfileInfoRegistrationForm(data = request.POST)
        print(str(request.POST))
        if user_form.is_valid() and profile_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.password = request.POST.get('password1')
            print(str(new_user))
            print("password before - "+str(new_user.password))
            # for (key,arg) in new_user:
            #     print(" key - {} arg - {}".format(key,arg))
            new_user.set_password(new_user.password)
            print("password after - "+str(new_user.password))
            new_user.save()
            profile = profile_form.save(commit=False)
            profile.user = new_user

            if 'profile_image' in request.FILES:
                profile.profile_image = req.FILES['profile_image']
            profile.save()

            registered = True
        else:
            err1 = profile_form.errors
            err2 = user_form.errors
            print(err1)
            print(err2)
            errors = True
    else:
        user_form = UserInfoRegistrationForm()
        profile_form = UserProfileInfoRegistrationForm()
    login_form = AuthenticationForm()

    return render(request,'registration/registration.html',{'registered':registered,'errors':errors,'reg_form1':user_form,'reg_form2':profile_form,'error_list1':err1,'error_list2':err2,'login_form':login_form})

#Creating a new Query Post
class CreateQueryPage(LoginRequiredMixin,CreateView):
    login_url = reverse_lazy('loginPageURL')
    form_class = web_app_forms.CreateQueryForm
    template_name = 'web_app/create_query2.html'
    model = forumModels.QueriesModel

    def form_valid(self,form):
        list_of_tags = form.instance.tags
        list_of_tags_trimmed = list_of_tags.split(",")[:5]
        print(list_of_tags_trimmed)
        list_of_tags_trimmed = [tag.strip() for tag in list_of_tags_trimmed]
        print(list_of_tags)
        print(list_of_tags_trimmed)
        if len(list_of_tags_trimmed) >= 1:
            if list_of_tags_trimmed[0] != '':
                form.instance.tag1 = forumModels.TagsModel.objects.get_or_create(tagName = list_of_tags_trimmed[0])[0]
        if len(list_of_tags_trimmed) >= 2:
            if list_of_tags_trimmed[1] != '':
                form.instance.tag1 = forumModels.TagsModel.objects.get_or_create(tagName = list_of_tags_trimmed[1])[0]
        if len(list_of_tags_trimmed) >= 3:
            if list_of_tags_trimmed[2] != '':
                form.instance.tag1 = forumModels.TagsModel.objects.get_or_create(tagName = list_of_tags_trimmed[2])[0]
        if len(list_of_tags_trimmed) >= 4:
            if list_of_tags_trimmed[3] != '':
                form.instance.tag1 = forumModels.TagsModel.objects.get_or_create(tagName = list_of_tags_trimmed[3])[0]
        if len(list_of_tags_trimmed) >= 5:
            if list_of_tags_trimmed[4] != '':
                form.instance.tag1 = forumModels.TagsModel.objects.get_or_create(tagName = list_of_tags_trimmed[4])[0]
        form.instance.created_by = self.request.user.profile
        return super(CreateQueryPage,self).form_valid(form)

    def get_context_data(self,**kwargs):
        context = super(CreateQueryPage,self).get_context_data(**kwargs)
        context['tags_list'] = forumModels.TagsModel.objects.all()
        context['search_bar_form'] = web_app_forms.SearchBarForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

    #success_url = reverse_lazy('web_app:ShowQueryPageURL',kwargs={'pk':pk_url_kwarg})

#DetailView of query
class ShowQueryPage(DetailView):
    template_name = 'web_app/query-new2.html'
    model = forumModels.QueriesModel
    context_object_name = 'query'

    def get_context_data(self, **kwargs):
        context = super(ShowQueryPage, self).get_context_data(**kwargs)
        context['query_discussion_form'] = web_app_forms.CreateDiscussionThreadsQueryForm()
        context['solution_discussion_form'] = web_app_forms.CreateDiscussionThreadsSolutionForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

class ShowQueryListByCategory(ListView):
    template_name = 'web_app/index-new2.html'

    model = forumModels.QueriesModel
    context_object_name = 'query_list'

    #
    def get_queryset(self):
        q_for_service = forumModels.ServicesModel.objects.filter(title__icontains=self.kwargs['category'].replace('-',' '))
        service = get_object_or_404(q_for_service)
        return forumModels.QueriesModel.objects.filter(service_area=service)
    def get_context_data(self,**kwargs):
        context = super(ShowQueryListByCategory,self).get_context_data(**kwargs)
        context['tags_list'] = forumModels.TagsModel.objects.all()
        context['search_bar_form'] = web_app_forms.SearchBarForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

class ShowSearchResult(ListView):
    template_name = 'web_app/index-new2.html'
    model = forumModels.QueriesModel
    context_object_name = 'query_list'

    def get_queryset(self):
        # + SearchVector('text') + SearchVector('service_area__title')
        search_vector = SearchVector('title')+ SearchVector('text') + SearchVector('service_area__title') + SearchVector('tags')
        search_query = SearchQuery(self.request.GET['search_query'])
        print(self.request.GET['search_query'])
        queryset = forumModels.QueriesModel.objects.annotate(rank=SearchRank(search_vector,search_query),search=search_vector).filter(search=self.request.GET['search_query']).order_by('-rank')
        return queryset
    def get_context_data(self,**kwargs):
        context = super(ShowSearchResult,self).get_context_data(**kwargs)
        context['tags_list'] = forumModels.TagsModel.objects.all()
        context['search_bar_form'] = web_app_forms.SearchBarForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        context['list_title'] = "Search Results -"
        return context

class UpdateQueryPage(LoginRequiredMixin,UpdateView):
    model = forumModels.QueriesModel
    fields = ['text','title','tags','last_modified']
    template_name = 'web_app/create_query2.html'

class UpdateAnswerPage(LoginRequiredMixin,UpdateView):
    model = forumModels.AnswersModel
    fields = ['text']
    template_name = 'web_app/create_query2.html'

class CreateAnswerPage(LoginRequiredMixin,CreateView):
    model = forumModels.AnswersModel
    form_class = web_app_forms.CreateAnswersForm
    template_name = 'web_app/create_answer2.html'

    def get_context_data(self,**kwargs):
        context = super(CreateAnswerPage,self).get_context_data(**kwargs)
        context['query'] = forumModels.QueriesModel.objects.get(pk=self.kwargs['querypk'])
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

    def form_valid(self,form):
        form.instance.created_by = self.request.user.profile
        form.instance.query = forumModels.QueriesModel.objects.get(pk=self.kwargs['querypk'])
        return super(CreateAnswerPage,self).form_valid(form)


class CreateDiscussionThreadsQueryPage(LoginRequiredMixin,CreateView):
    model = forumModels.DiscussionThreadsQueryModel
    form_class = web_app_forms.CreateDiscussionThreadsQueryForm
    template_name = 'web_app/query-new2.html'


    def form_valid(self, form):
        form.instance.created_by = self.request.user.profile
        form.instance.query = forumModels.QueriesModel.objects.get(pk=self.kwargs['querypk'])
        return super(CreateDiscussionThreadsQueryPage, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ShowQueryPage, self).get_context_data(**kwargs)
        context['query_discussion_form'] = web_app_forms.CreateDiscussionThreadsQueryForm()
        context['solution_discussion_form'] = web_app_forms.CreateDiscussionThreadsSolutionForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

class CreateDiscussionThreadsSolutionPage(LoginRequiredMixin,CreateView):
    model = forumModels.DiscussionThreadsSolutionModel
    form_class = web_app_forms.CreateDiscussionThreadsSolutionForm
    template_name = 'web_app/query-new2.html'


    def form_valid(self, form):
        form.instance.created_by = self.request.user.profile
        form.instance.ans = forumModels.AnswersModel.objects.get(pk=self.kwargs['answerpk'])
        return super(CreateDiscussionThreadsSolutionPage, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(ShowQueryPage, self).get_context_data(**kwargs)
        context['query_discussion_form'] = web_app_forms.CreateDiscussionThreadsQueryForm()
        context['solution_discussion_form'] = web_app_forms.CreateDiscussionThreadsSolutionForm()
        context['category_list'] = forumModels.ServicesModel.objects.all()
        return context

# Upvote and downvote
class UpdateUpvote(DetailView):
    template_name = 'web_app/query-new2.html'
    model = forumModels.QueriesModel
    context_object_name = 'query'

    def get_context_data(self, **kwargs):
        pk = self.kwargs['answerpk']
        answer = forumModels.AnswersModel.objects.get(pk=pk)
        is_upvoted = forumModels.UpvotesModel.objects.get_or_create(upvoter=self.request.user.profile,upvoted_answer=answer)
        if is_upvoted[1]:
            answer.upvotes += 1
            answer.save()
        else:
            answer.upvotes -= 1
            answer.save()
            is_upvoted[0].delete()

        context = super(UpdateUpvote, self).get_context_data(**kwargs)
        context['query_discussion_form'] = web_app_forms.CreateDiscussionThreadsQueryForm()
        context['solution_discussion_form'] = web_app_forms.CreateDiscussionThreadsSolutionForm()
        return context

class UpdateDownvote(DetailView):
    template_name = 'web_app/query-new2.html'
    model = forumModels.QueriesModel
    context_object_name = 'query'

    def get_context_data(self, **kwargs):
        pk = self.kwargs['answerpk']
        answer = forumModels.AnswersModel.objects.get(pk=pk)
        is_downvoted = forumModels.DownvotesModel.objects.get_or_create(downvoter=self.request.user.profile,downvoted_answer=answer)
        if is_downvoted[1]:
            answer.downvotes += 1
            answer.save()
        else:
            answer.downvotes -= 1
            answer.save()
            is_downvoted[0].delete()

        context = super(UpdateDownvote, self).get_context_data(**kwargs)
        context['query_discussion_form'] = web_app_forms.CreateDiscussionThreadsQueryForm()
        context['solution_discussion_form'] = web_app_forms.CreateDiscussionThreadsSolutionForm()
        return context

# We do not think deleting should be an option in forums even for editors ( apart from admin/superuser)
# class DeleteQueryPage(LoginRequiredMixin,DeleteView):
#     pass

# unpublish the Question
class UserDashboardPage(LoginRequiredMixin,TemplateView):
    pass

class AnalyticsPage(LoginRequiredMixin,TemplateView):
    pass

@login_required
def exportCSVtrainData(request):
    return export_to_csv()

@login_required
def populate_train_db(request):
    save_train_data()
    return HttpResponse('<h1>Success</h1>')
