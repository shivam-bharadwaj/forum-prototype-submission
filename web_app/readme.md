# The Web Interface

This App is supposed to serve the Web Based interface for the Project's backend.
The Directory contains important script files such as _forms.py_ , _urls.py_ , '_train.py_' _sentiment_analysis.py_' and the _views.py_

* *train.py* - is responsible for populating the TrainDataSet table for training of Machine Learning Model Required to generate the Summary of Long Query Answers.

* *sentiment_analysis.py* - It is a test script to fetch the twitter data for storing and performing analytics.

* *forms.py* - handles what forms will be there in throughout the project

* *urls.py* - It stores all the urls with their regex pattern to serve web_app

* *views.py* - It is supposed to handle incoming requests on the Web Interface, the similar will be implemented in open_apis app.
