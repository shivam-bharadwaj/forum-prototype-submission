from django import forms
from app_models import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User



class UserInfoRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username','first_name','last_name','email','password1','password2')
class UserProfileInfoRegistrationForm(forms.ModelForm):
    class Meta:
        model = models.UserProfileModel
        fields = ('profile_image',)

class CreateQueryForm(forms.ModelForm):

    class Meta:
        model = models.QueriesModel
        # exclude = ('text',)
        exclude = ('created_by','created_date','last_modified','is_spam','tag1','tag2','tag3','tag4','tag5','answer_summary')

        widgets = {
        'title':forms.TextInput(attrs={'label':'Your Query'}),
        }

class CreateTagsForm(forms.ModelForm):
    class Meta:
        model = models.TagsModel
        fields = '__all__'
class CreateAnswersForm(forms.ModelForm):
    class Meta:
        model = models.AnswersModel
        fields = ('text',)
class CreateDiscussionThreadsSolutionForm(forms.ModelForm):
    class Meta:
        model = models.DiscussionThreadsSolutionModel
        fields =('text',)


        widgets = {
        'text' : forms.TextInput(attrs={'class':'textinputclass','placeholder':'enter comment to discuss on solution'})
        }

class CreateDiscussionThreadsQueryForm(forms.ModelForm):
    class Meta:
        model = models.DiscussionThreadsQueryModel
        fields = ('text',)

        widgets = {
        'text' : forms.TextInput(attrs={'class':'textinputclass','placeholder':'enter comment to discuss question'})
        }

class CreateServiceForm(forms.ModelForm):
    class Meta:
        model = models.ServicesModel
        fields = '__all__'


## other Functionality forms
class SearchBarForm(forms.Form):
    search_query = forms.CharField(label='Search',widget=forms.TextInput(attrs={'class':'search_bar_class','placeholder':'search the forum'}))
