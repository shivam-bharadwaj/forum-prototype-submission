from app_models.models import QueriesModel,AnswersModel,TrainDataSet
from html.parser import HTMLParser
import csv
from django.http import HttpResponse
from django.http import StreamingHttpResponse

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs= True
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)

def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """
    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def save_data_for_summary_generation():
    print("started function - ")
    queries_objects = QueriesModel.objects.filter(answer_summary = None)
    print(queries_objects)
    for q in  queries_objects:
        all_ans_combined = 'All the Answers Include:\n'
        all_ans = q.answers.all()
        counter = 1
        for a in all_ans:
            print("printing answer")
            stripped_a = strip_tags(str(a))
            # print(stripped_a)
            all_ans_combined += str(counter) +". "+ stripped_a + '\n'
            counter = counter + 1
        print(all_ans_combined)

def save_train_data():
    print("started function - ")
    queries_objects = QueriesModel.objects.exclude(answer_summary = None)
    print(queries_objects)
    for q in  queries_objects:
        all_ans_combined = 'All the Answers Include:\n'
        all_ans = q.answers.all()
        counter = 1
        for a in all_ans:
            print("printing answer")
            stripped_a = strip_tags(str(a))
            # print(stripped_a)
            all_ans_combined += str(counter) +". "+ stripped_a + '\n'
            counter = counter + 1
        print(all_ans_combined)
        try:
            new_obj = TrainDataSet.objects.get(query_pk = q)
        except TrainDataSet.DoesNotExist:
            new_obj = TrainDataSet(query_pk = q,summary=q.answer_summary,answer=all_ans_combined)
            new_obj.save()

def export_to_csv():
    """ Returns te reference to CSV file just Created for web_download"""


    fields = list()
    fields.append(TrainDataSet._meta.get_field('query_pk'))
    fields.append(TrainDataSet._meta.get_field('summary'))
    fields.append(TrainDataSet._meta.get_field('answer'))
    rows = list()

    writer = csv.writer(open("all_train_data.csv", 'w+',  newline='', encoding='utf-8'))

    for obj in TrainDataSet.objects.all():
        row=""
        # for field in fields:
        #     row += str(getattr(obj,field.name)) + ','
        #     print("writing row - {}".format(row))
        row += str(obj.query_pk.pk) + ","
        row += str(obj.summary) + ","
        row += str(obj.answer) + ","

        rows.append(row)
    print(row)
    response = StreamingHttpResponse((writer.writerow(rows) for row in rows),
                                     content_type="text/csv")
    response['Content-Disposition'] = 'attachment; filename="all_train_data.csv"'
    return response
