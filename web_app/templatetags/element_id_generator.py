from django import template

register = template.Library()

@register.filter
def generate_id(value,arg):
    string_pk = str(value)
    return arg+string_pk
